﻿namespace CodeGenerator

module CodeGen =
    open System

    let capitalizeFirstLetter (s: string) =
        s.Substring(0, 1).ToUpper() + s.Substring(1);
    
    let addDash (s: string) =
        "_" + s
    
    let GenField (datatype: string) (name: string) =
        sprintf "private %s %s;\n" datatype (addDash name)
    
    let GenGet (name: string) =
        sprintf "get { return %s; }\n" (addDash name)
    
    let GenSet (name: string) =
        sprintf "set \n{\n\t%s = value;\n\tRaisePropertyChanged(\"%s\");\n}\n" (addDash name) (capitalizeFirstLetter name)
    
    let GenAccessors (datatype: string) (name: string) =
        let header = sprintf "public %s %s\n{\n" datatype (capitalizeFirstLetter name)
        let footer = "}\n\n"
        let inside = (GenGet name) + (GenSet name)
        header + inside + footer
    
    let Encapsulate (datatype: string) (name: string) =
        (GenField datatype name) + "\n" + (GenAccessors datatype name)
    
    let Generate (input: string) =
        let entries = input.Split([|';'|], StringSplitOptions.RemoveEmptyEntries)
        (Array.map ((fun (entry: string) -> entry.Split([|' '|], StringSplitOptions.RemoveEmptyEntries)) >> 
            (fun (entry: string array) -> Encapsulate entry.[0] entry.[1])) entries)
        |> Array.fold (fun acc el -> acc + "\n" + el) ""
    
    // Generate "string hui; int penis; double huyauble"
    
    //Generate "string artist; string album; string title; int year; TimeSpan duration; int trackNo; StorageFile file; string lyrics; Lyrics syncedLyrics"
    
    // Generate "ObservableCollection<Track> tracks"
    
    // Generate "string name; ObservableCollection<Track> tracks; string artist; StorageItemThumbnail art"
    
    // Generate "ObservableCollection<Track> tracks; ObservableCollection<Album> albums; ObservableCollection<Artists> artists;"
    
    let registerDp (typename: string) (name: string) (classname: string) =
        let sname = capitalizeFirstLetter name
        sprintf "public static readonly DependencyProperty %sProperty = \n\tDependencyProperty.Register(\"%s\", typeof(%s), typeof(%s), null);"
            sname sname typename classname
    
    let encapsulateDp (typename: string) (name: string) =
        let sname = capitalizeFirstLetter name
        let get = sprintf "\tget { return (%s)GetValue(%sProperty); }\n" typename sname
        let set = sprintf "\tset { SetValue(%sProperty, value); }\n" sname
        sprintf "public %s %s\n{\n%s%s}\n" typename sname get set
    
    let createDp (typename: string) (name: string) (classname: string) =
        let reg = registerDp typename name classname
        let enc = encapsulateDp typename name
        reg + "\n\n" + enc
    
    let genDp (input: string) =
        let divide = input.Split([|':'|], StringSplitOptions.RemoveEmptyEntries)
        let classname = divide.[0]
        let entries = divide.[1].Split([|';'|], StringSplitOptions.RemoveEmptyEntries)
        (Array.map ((fun (entry: string) -> entry.Split([|' '|], StringSplitOptions.RemoveEmptyEntries)) >> 
            (fun (entry: string array) -> createDp entry.[0] entry.[1] classname)) entries)
        |> Array.fold (fun acc el -> acc + "\n" + el) ""
    
    // genDp "ArtistDetails: Artist Artist"
    
    (* __________________________ SINGLETON _____________________ *)
    
    let singleton name =
        let bigname = capitalizeFirstLetter name
        let smallname = addDash name
    
        "\n#region Singleton\n" +
            (sprintf "private %s() { }\n" bigname) +
            (sprintf "private static  %s %s;\n\n" bigname smallname) +
            (sprintf "public static %s Instance\n{\n" bigname) +
            "\tget\n\t{\n" +
            (sprintf "\t\tif (%s == null)\n" smallname) +
            "\t\t{\n" +
            (sprintf "\t\t\t%s = new %s();\n" smallname bigname) +
            "\t\t}\n" +
            (sprintf "\t\treturn %s;\n" smallname) +
            "\t}\n}\n#endregion\n"
