﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using CodeGenerator;
using System.Collections.ObjectModel;
using System.Text;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace UWPCodeGenerator
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            Dps = new ObservableCollection<TwoStrings>();
            Props = new ObservableCollection<TwoStrings>();
        }

        public static readonly DependencyProperty DpsProperty =
            DependencyProperty.Register("Dps", typeof(ObservableCollection<TwoStrings>), typeof(MainPage), null);

        public ObservableCollection<TwoStrings> Dps
        {
            get { return (ObservableCollection<TwoStrings>)GetValue(DpsProperty); }
            set { SetValue(DpsProperty, value); }
        }

        public static readonly DependencyProperty PropsProperty =
            DependencyProperty.Register("Props", typeof(ObservableCollection<TwoStrings>), typeof(MainPage), null);

        public ObservableCollection<TwoStrings> Props
        {
            get { return (ObservableCollection<TwoStrings>)GetValue(PropsProperty); }
            set { SetValue(PropsProperty, value); }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var tb = sender as TextBox;
            try
            {
                var str = CodeGen.singleton(tb.Text);
                SingletonResult.Text = str;
            }
            catch (Exception ex)
            {
                SingletonResult.Text = ex.Message;
            }
        }

        private void UpdateDP(object sender, TextChangedEventArgs e)
        {
            var sb = new StringBuilder();
            foreach (var twostr in this.Dps)
            {
                try
                {
                    var dp = CodeGen.createDp(twostr.Typename, twostr.Name, Ui.Text);
                    sb.AppendLine(dp);
                }
                catch (Exception ex)
                {
                    DpResult.Text = ex.Message;
                }
            }

            DpResult.Text = sb.ToString();
        }

        private void AddNewDP(object sender, RoutedEventArgs e)
        {
            this.Dps.Add(new TwoStrings() { Name = "DePro", Typename = "string" });
        }

        private void AddNewProp(object sender, RoutedEventArgs e)
        {
            this.Props.Add(new TwoStrings() { Name = "ProPro", Typename = "int" });
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            UpdateDP(null, null);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var sb = new StringBuilder();
            foreach (var twostr in this.Props)
            {
                try
                {
                    var dp = CodeGen.Encapsulate(twostr.Typename, twostr.Name);
                    sb.AppendLine(dp);
                }
                catch (Exception ex)
                {
                    PropsResult.Text = ex.Message;
                }
            }

            PropsResult.Text = sb.ToString();
        }
    }
}
